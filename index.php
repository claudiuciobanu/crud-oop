<table id="mytable" class="table table-bordred table-striped">
<thead>
<th>#</th>
<th>Nume</th>
<th>Prenume</th>
<th>Adresa</th>
<th>Departament</th>

<th>Edit</th>
<th>Delete</th>
</thead>
<tbody>

<?php
require_once("Functions.php");
 $fetchdata=new Crud();
  $sql=$fetchdata->fetchdata();
  $cnt=1;
  while($row=mysqli_fetch_array($sql))
  {
  ?>
    <tr>
    <td><?php echo htmlentities($cnt);?></td>
    <td><?php echo htmlentities($row['nume']);?></td>
    <td><?php echo htmlentities($row['prenume']);?></td>
    <td><?php echo htmlentities($row['adresa']);?></td>
    <td><?php echo htmlentities($row['departament']);?></td>

    <td><a href="update.php?id=<?php echo htmlentities($row['id']);?>"><button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></button></a></td>
    <td><a href="delete.php?del=<?php echo htmlentities($row['id']);?>"><button class="btn btn-danger btn-xs" onClick="return 
    confirm('Do you really want to delete');"><span class="glyphicon glyphicon-trash"></span></button></a></td>
    <td><a href = "Insert.php?insert=<?php echo htmlentities($row['id']);?>"  
    </tr>
<?php

$cnt++;
} ?>
<td>
<a href = "Insert.php">Create</a>
</td>
</tbody>
</table>




<?php
require_once'Functions.php';
$insertdata=new Crud();
if(isset($_POST['insert']))
{

$nume=$_POST['nume'];
$prenume=$_POST['prenume'];
$adresa=$_POST['adresa'];
$departament=$_POST['departament'];
$sql=$insertdata->insert($nume,$prenume,$adresa,$departament);
if($sql)
{
echo "<script>alert('Record inserted successfully');</script>";
echo "<script>window.location.href='index.php'</script>";
}
else
{
echo "<script>alert('Something went wrong. Please try again');</script>";
echo "<script>window.location.href='index.php'</script>";
}
}
?>